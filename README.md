# BigBlueButton Ansible Role

The [tasks/install.yml](tasks/install.yml) steps follow [the BigBlueButton 2.2
Install page](https://docs.bigbluebutton.org/2.2/install.html).

The [tasks/greenlight.yml](tasks/greenlight.yml) steps follow [the Greenlight
Install page](https://docs.bigbluebutton.org/greenlight/gl-install.html).

The role will need refactoring when there is a version other than 2.2 on Xenial
to support.
 
