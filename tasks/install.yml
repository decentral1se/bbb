---
- name: Follow https://docs.bigbluebutton.org/2.2/install.html
  block:

    - name: Install packages
      apt:
        pkg:
          - apt-transport-https
          - haveged
          - language-pack-en
          - pwgen

    - name: Generate Locale
      locale_gen:
        name: en_US.UTF-8
        state: present

    - name: Check the environment
      command: systemctl show-environment
      register: bbb_systemctlenv
      changed_when: false
      check_mode: false

    - name: Set the language
      command: systemctl set-environment LANG=en_US.UTF-8
      when: ( "LANG=en_US.UTF-8" not in bbb_systemctlenv.stdout_lines )

    - name: Apt repos present
      apt_repository:
        repo: "{{ repo }}"
        state: present
        update_cache: true
      loop:
        - ppa:bigbluebutton/support
        - ppa:rmescandon/yq
      loop_control:
        loop_var: repo

    - name: Dist upgrade
      apt:
        upgrade: dist
        update_cache: false
        autoclean: true
        autoremove: true

    - name: BBB gpg key present
      apt_key:
        url: "https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc"
        state: present

    - name: BBB apt repo enabled
      apt_repository:
        repo: "deb https://ubuntu.bigbluebutton.org/xenial-22/ bigbluebutton-xenial main"
        state: present
        filename: bigbluebutton
        update_cache: true

    - name: Install BBB
      apt:
        pkg:
          - bigbluebutton

    - name: Install BBB HTML5 interface
      apt:
        pkg:
          - bbb-html5

    - name: BBB demo package purged
      apt:
        pkg:
          - bbb-demo
        purge: true
        state: absent
      when: not bbb_demo

    - name: Dist upgrade
      apt:
        upgrade: dist
        update_cache: false
        autoclean: true
        autoremove: true

    - name: BBB hostname set
      command: "bbb-conf --setip {{ inventory_hostname }}"

    - name: Ngnix config in place
      template:
        src: bigbluebutton.j2
        dest: /etc/nginx/sites-available/bigbluebutton
        backup: true

    - name: Nginx websocket forwarding in place
      template:
        src: sip.nginx.j2
        dest: /etc/bigbluebutton/nginx/sip.nginx
        backup: true

    - name: securitySalt in place
      shell: pwgen -n 43 1 > /root/.bbb.securitysalt
      args:
        creates: /root/.bbb.securitysalt
      no_log: true

    - name: securitySalt slurped
      slurp:
        src: /root/.bbb.securitysalt
      register: bbb_securitysalt_b64encoded
      no_log: true

    - name: Variables set for the securitySalt
      set_fact:
        bbb_securitysalt: "{{ bbb_securitysalt_b64encoded['content'] | b64decode | trim }}"
      no_log: true

    - name: bigbluebutton.properties in place
      template:
        src: bigbluebutton.properties.j2
        dest: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
        backup: true

    - name: screenshare.properties in place
      template:
        src: screenshare.properties.j2
        dest: /usr/share/red5/webapps/screenshare/WEB-INF/screenshare.properties
        owner: red5
        group: red5
        mode: 0744
        backup: true

    - name: config.xml in place
      template:
        src: config.xml.j2
        dest: /var/www/bigbluebutton/client/conf/config.xml
        backup: true

    - name: meteor settings in place
      template:
        src: settings.yml.j2
        dest: /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
        owner: meteor
        group: meteor
        mode: 0444
        backup: true

    - name: bigbluebutton.yml recording setting in place
      template:
        src: bigbluebutton.yml.j2
        dest: /usr/local/bigbluebutton/core/scripts/bigbluebutton.yml
        mode: 0755
        backup: true

    # https://docs.bigbluebutton.org/2.2/customize.html
    - name: UFW present
      apt:
        pkg:
          - ufw
        state: present

    - name: UFW TCP configured
      ufw:
        name: "{{ rule }}"
        rule: allow
      loop:
        - OpenSSH
        - Nginx Full
      loop_control:
        loop_var: rule

    - name: UFW UDP configured
      ufw:
        port: "{{ port }}"
        rule: allow
        proto: udp
      loop:
        - 16384:32768
      loop_control:
        loop_var: port

    - name: UFW enabled
      service:
        name: ufw
        enabled: true

    - name: BBB checked
      command: bbb-conf --check

    - name: BBB restarted
      command: bbb-conf --restart

  tags:
    - bbb
...
